-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: numeta
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `competition_history`
--

DROP TABLE IF EXISTS `competition_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition_history` (
  `competition_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_competition` datetime NOT NULL,
  `team_id` int(11) NOT NULL,
  `tournament` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placement` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`competition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competition_history`
--

LOCK TABLES `competition_history` WRITE;
/*!40000 ALTER TABLE `competition_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `competition_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `following`
--

DROP TABLE IF EXISTS `following`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `following` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `status_del` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `following`
--

LOCK TABLES `following` WRITE;
/*!40000 ALTER TABLE `following` DISABLE KEYS */;
/*!40000 ALTER TABLE `following` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_stats`
--

DROP TABLE IF EXISTS `game_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_stats` (
  `game_stats_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `best_weapon_class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `headshot` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accuracy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `high_solo_rank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kill_death_ratio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `winrate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preferred` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`game_stats_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_stats`
--

LOCK TABLES `game_stats` WRITE;
/*!40000 ALTER TABLE `game_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2019_11_08_090804_create_profile_table',1),(9,'2019_11_08_091543_create_game_stats_table',1),(10,'2019_11_08_091835_create_competition_history_table',1),(11,'2019_11_08_231210_create_following_table',2),(12,'2019_11_08_232559_create_table_teams_table',2),(13,'2019_11_09_135620_create_table_team_transaction_table',3),(14,'2019_11_09_145919_create_table_tourney_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('054296e0f318bb6a21029c55d98afed77df350064d8facbd78655acc60ea92ccfbd28d7c27662c01',1,1,'createToken','[]',0,'2019-11-09 19:10:42','2019-11-09 19:10:42','2020-11-10 02:10:42'),('08833923d68bde7d77f0b02cd541b47eecabbcfb01889d9fa89dba5a9272a00942ba3050a995eec1',1,1,'createToken','[]',0,'2019-11-09 06:40:14','2019-11-09 06:40:14','2020-11-09 13:40:14'),('180aba068983ed3fb76b68644f830d198badeaec507af0382505036304ded1aad5ddc80e267801a4',1,1,'createToken','[]',0,'2019-11-08 19:47:08','2019-11-08 19:47:08','2020-11-09 02:47:08'),('1af6d68b5ae571d214fa243b8c9ab9035bd6ba11508c78d5cbdc2ca3353798c21327a66f804d23f4',1,1,'createToken','[]',0,'2019-11-09 19:05:17','2019-11-09 19:05:17','2020-11-10 02:05:17'),('1d041d2b5ec80e26f7fc1908b2e5a8f4f863921acb7196cf6f04ad30e9b296e4169c050db2e43edc',1,1,'createToken','[]',0,'2019-11-08 19:47:35','2019-11-08 19:47:35','2020-11-09 02:47:35'),('20ae61e31be2cc64f4a188e32193143f76daf811e310a01d2708cab3cbd3047579ada4b148a3e6e3',1,1,'Laravel Password Grant Client','[]',0,'2019-11-08 02:55:25','2019-11-08 02:55:25','2020-11-08 09:55:25'),('21d6f6656c39afad3e06124e8c9110b31470cc79bab1c381fed58f6d658f5a936be38f667a59ac5c',1,1,'createToken','[]',0,'2019-11-08 19:46:00','2019-11-08 19:46:00','2020-11-09 02:46:00'),('287aab94573b2b2c67399aa5c19959b17881e0aeff97ef100397bc4e5ddb82fc386db1d2b2dd936b',1,1,'createToken','[]',0,'2019-11-09 19:48:21','2019-11-09 19:48:21','2020-11-10 02:48:21'),('3bc764a375c60ab0154cec744ab8dbfeb77785beef052bf14603c27abf4fcf3ea1b4cfe2a016f9be',1,1,'createToken','[]',0,'2019-11-08 19:45:57','2019-11-08 19:45:57','2020-11-09 02:45:57'),('3edf4e66cd485aee30fd2006509253dc641f2c8e53ccc14dea16104aa3db8272276195b90aadf9de',1,1,'createToken','[]',0,'2019-11-09 19:07:03','2019-11-09 19:07:03','2020-11-10 02:07:03'),('41ef1ff118dffc929b6f1236f1c8f10de9cbd9cafe8689c8a5a19f9bae4606755860b20a5e2d9fc5',2,1,'Laravel Password Grant Client','[]',0,'2019-11-08 08:31:00','2019-11-08 08:31:00','2020-11-08 15:31:00'),('4409ae12b46ffbf752e290402ad0617580f52f252e39fc807fa94b3649144dfc2caac866e3558057',1,1,'createToken','[]',0,'2019-11-08 19:43:13','2019-11-08 19:43:13','2020-11-09 02:43:13'),('45d143a9eb2408f56d36edc37e540aad8a52326e354d6bc9f5a8e942b81b8f70dd5129af8e434b73',1,1,'createToken','[]',0,'2019-11-09 19:10:14','2019-11-09 19:10:14','2020-11-10 02:10:14'),('4e1c68c8dd95ad1ec8fdcd7153dbe6c12baed6fbc89bd5c227ac75cd2269627c004a9601b2321cf5',1,1,'createToken','[]',0,'2019-11-09 19:11:48','2019-11-09 19:11:48','2020-11-10 02:11:48'),('64890ba72274d32b87f928f27e6e62fe6586cf20eac748a0197c13b86f68d24c54548ed5b6d30e05',1,1,'createToken','[]',0,'2019-11-09 20:13:44','2019-11-09 20:13:44','2020-11-10 03:13:44'),('7b72a9ee54876362c04cc339b4eefc866580faaea306cd681ae601a99263c7cdea043c5de709f9ac',1,1,'createToken','[]',0,'2019-11-08 16:38:01','2019-11-08 16:38:01','2020-11-08 23:38:01'),('7df1f788df8d78756c70b53f0691570fe9b3a9d87e0ca4441878b217dcbe7139cab7295ac7f68448',1,1,'createToken','[]',0,'2019-11-09 19:00:10','2019-11-09 19:00:10','2020-11-10 02:00:10'),('94ef7696dde110ab2225514ce36a436c9d7adffdf3159098f5376bbc9df688ad2089584e071c12ff',1,1,'createToken','[]',0,'2019-11-08 02:57:39','2019-11-08 02:57:39','2020-11-08 09:57:39'),('bfe7d60a85ba673e3ddbb9413afc9f3ac47886c249854a57f508d5d4736e1eef784a5922f6d9462a',1,1,'createToken','[]',0,'2019-11-09 18:59:31','2019-11-09 18:59:31','2020-11-10 01:59:31'),('c6ef6f09f4c643b1042bcb024e95af34142caa3fd08f29f0068f82c2ea28e117af0e99cb68115fc0',1,1,'createToken','[]',0,'2019-11-09 19:50:52','2019-11-09 19:50:52','2020-11-10 02:50:52'),('db8c3afdd94280d2252636da03ce20cd8fcce87ae76a3d7cdf22c854421997ec4a214418e08e3dea',1,1,'createToken','[]',0,'2019-11-09 19:06:10','2019-11-09 19:06:10','2020-11-10 02:06:10'),('dd5fdd400354ced09c45fea3187fb78a1388793ef69ce1f6b287b16f3c3438be79957c516af24bcb',1,1,'createToken','[]',0,'2019-11-09 19:06:22','2019-11-09 19:06:22','2020-11-10 02:06:22'),('e6d7cb96c4f40adaff9e3a17879d186fb08c8ea678296114b1c0d291b19c02e70963341cd5716959',1,1,'createToken','[]',0,'2019-11-08 02:56:35','2019-11-08 02:56:35','2020-11-08 09:56:35'),('f70c4461dde2d49872c625adaa3e1f630727e74f559b9e4bd3e4aef802453311ef0b0083f4fe84e4',1,1,'createToken','[]',0,'2019-11-08 19:47:10','2019-11-08 19:47:10','2020-11-09 02:47:10');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','4a53HYaXpEAusAaHCMbzqbod9lpRDryRga2m3OCr','http://localhost',1,0,0,'2019-11-08 02:53:42','2019-11-08 02:53:42'),(2,NULL,'Laravel Password Grant Client','WzuosVMAtDMZydCls30VkcZQo7MmQiHO8VFzBxCX','http://localhost',0,1,0,'2019-11-08 02:53:42','2019-11-08 02:53:42'),(3,NULL,'Laravel Personal Access Client','LiPKeLBkgj8zd9h6kmr3k5d4yLfjIOSOXbyg5PG0','http://localhost',1,0,0,'2019-11-19 06:40:22','2019-11-19 06:40:22'),(4,NULL,'Laravel Password Grant Client','aOqk7HweGxJtd4d1ve36cIKUrrqrD8HZUU7bociA','http://localhost',0,1,0,'2019-11-19 06:40:23','2019-11-19 06:40:23');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2019-11-08 02:53:42','2019-11-08 02:53:42'),(2,3,'2019-11-19 06:40:23','2019-11-19 06:40:23');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `id_profile` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `photo_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `steam_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `followers` int(11) DEFAULT NULL,
  `connections` int(11) DEFAULT NULL,
  `rep_point` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photos_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` int(11) DEFAULT NULL,
  `steam_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,1,'ww.imgae','masih kosong',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'2019-11-08 16:39:36'),(2,2,'ww.imgae','masih kosong biodatanya',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-08 08:31:00','2019-11-09 21:00:34');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_team_transaction`
--

DROP TABLE IF EXISTS `table_team_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_team_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) NOT NULL,
  `id_users` bigint(20) NOT NULL,
  `status_del` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_team_transaction`
--

LOCK TABLES `table_team_transaction` WRITE;
/*!40000 ALTER TABLE `table_team_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_team_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_teams`
--

DROP TABLE IF EXISTS `table_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_teams` (
  `team_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_team` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `captain_team` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_del` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_teams`
--

LOCK TABLES `table_teams` WRITE;
/*!40000 ALTER TABLE `table_teams` DISABLE KEYS */;
INSERT INTO `table_teams` VALUES (1,'numete','1',NULL,'2019-11-09 06:43:23','2019-11-09 06:43:23'),(2,'numete',NULL,NULL,'2019-11-09 19:49:27','2019-11-09 19:49:27'),(3,'numeta','1',0,'2019-11-09 20:21:57','2019-11-09 20:21:57');
/*!40000 ALTER TABLE `table_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_tourney`
--

DROP TABLE IF EXISTS `table_tourney`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_tourney` (
  `tourney_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_tournamen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `game` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe_game` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_participant` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tourney_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_tourney`
--

LOCK TABLES `table_tourney` WRITE;
/*!40000 ALTER TABLE `table_tourney` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_tourney` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'arif','hkyotori@gmail.com',NULL,'$2y$10$/gQ9hCHZwwuRPtnQ69RaE.kp2hh/MRk3EmSMWwTle5j.ecH6Lsg/W',NULL,NULL,NULL,NULL,NULL,'2019-11-08 02:55:25','2019-11-08 02:55:25'),(2,'pras','prasetya@gmail.com',NULL,'$2y$10$to9bZQ8yQPbejh2SHJjIHe6pENWCvLdfwxfv5za8ojrJm8MfyYtDi',NULL,NULL,NULL,NULL,NULL,'2019-11-08 08:31:00','2019-11-08 08:31:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'numeta'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-29 20:51:06
