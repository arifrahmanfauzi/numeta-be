<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_history', function (Blueprint $table) {
            $table->bigIncrements('competition_id');
            $table->dateTime('date_competition')->nullable();
            $table->integer('team_id')->nullable();
            $table->string('tournament')->nullable();
            $table->string('placement')->nullable();
            $table->smallInteger('status_del');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_history');
    }
}
