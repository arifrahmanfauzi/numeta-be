<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_stats', function (Blueprint $table) {
            $table->bigIncrements('game_stats_id');
            $table->string('best_weapon_class');
            $table->string('headshot');
            $table->string('accuracy');
            $table->string('high_solo_rank');
            $table->string('kill_death_ratio');
            $table->string('winrate');
            $table->string('position');
            $table->string('preferred');
            $table->smallInteger('status_del');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_stats');
    }
}
