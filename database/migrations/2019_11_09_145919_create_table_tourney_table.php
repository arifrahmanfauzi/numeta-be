<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTourneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_tourney', function (Blueprint $table) {
            $table->bigIncrements('tourney_id');
            $table->string('nama_tournamen')->nullable();
            $table->string('game')->nullable();
            $table->string('tipe_game')->nullable();
            $table->integer('max_participant')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_tourney');
    }
}
