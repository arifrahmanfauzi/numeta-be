<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['json.response']], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    // public routes
    Route::post('/login', 'Api\AuthController@login')->name('login.api');
    Route::post('/register', 'Api\AuthController@register')->name('register.api');
    Route::get('/listuser', 'User\UserController@list_user');
    Route::get('user/{id}', 'User\UserController@get_specified_user');

    //Teams Dota Api
    Route::get('/getTeams/{id?}', 'OpenDotaController@fetchTeams');
    Route::get('/getTeams/{id}/matches', 'OpenDotaController@fetchTeamMatch');
    Route::get('/getProPlayers', 'OpenDotaController@fetchProPlayer');
    
    //Profile & User Endpoints
    Route::get('profile/{id}', 'User\UserController@get_specified_profile');
    Route::put('/profileupdate/{id}','User\UserController@update_profile');
    Route::get('/profile', 'User\UserController@get_all_user');
    Route::get('/getprofile', 'User\UserController@getProfile');
    
    //Teams Controller
    Route::get('/team', 'Team\TeamController@index');
    Route::put('/team/{id}', 'Team\TeamController@update_team');
    Route::delete('/team/{id}', 'Team\TeamController@delete_team');
    
    
    // private routes
    Route::middleware('auth:api')->group(function () {
        
        // Profile

        // Team
        Route::post('/team', 'Team\TeamController@create_team');


        Route::get('/logout', 'Api\AuthController@logout')->name('logout');
    });

});
