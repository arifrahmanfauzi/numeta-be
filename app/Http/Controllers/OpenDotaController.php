<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\Message\Response;

use function GuzzleHttp\json_decode;

class OpenDotaController extends Controller
{
    public function fetchProPlayer(Client $client){
        $request = $client->get('https://api.opendota.com/api/proPlayers');
        $response = $request->getBody()->getContents();
        $result = json_decode($response, true);

        return response()->json($result);
    }

    public function fetchTeams($id = null, Client $client){
        if (!$id) {
            $request = $client->get('https://api.opendota.com/api/teams');
            $response = $request->getBody()->getContents();
            $result = json_decode($response, true);

            return response()->json($result);
        } else {
            $request = $client->get('https://api.opendota.com/api/teams/'.$id);
            $response = $request->getBody()->getContents();
            $result = json_decode($response, true);

            return response()->json($result);
        }
    }

    public function fetchTeamMatch($id, Client $client){
            $request = $client->get('https://api.opendota.com/api/teams/'.$id.'/matches');
            $response = $request->getBody()->getContents();
            $result = json_decode($response, true);

            return response()->json($result);
        }
}
