<?php

namespace App\Http\Controllers\Tourney;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Tourney;

class TourneyController extends Controller
{
    public function index(){

    }

    public function create_tournament(Request $req){
        $tourney = new Tourney;

        $tourney->nama_tournamen = $req->nama_tournament;
        $tourney->game = $req->game;
        $tourney->tipe_game = $req->tipe_game;
        $tourney->max_participant = $req->max_participant;
        $tourney->save();

        return response(['status' => 'success', 'message' => 'tourney has been created', 'data' => $tourney], 200);

    }
    public function update_tournament($id){
        $tourney = Tourney::findOrFail($id);
        $tourney->nama_tournamen = $req->nama_tournament;
        $tourney->game = $req->game;
        $tourney->tipe_game = $req->tipe_game;
        $tourney->max_participant = $req->max_participant;
        $tourney->save();

        return response(['status' => 'success', 'message' => 'tournament has been updated', 'data' => $tourney], 200);
    }
}
