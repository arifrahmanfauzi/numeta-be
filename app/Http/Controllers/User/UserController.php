<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Profile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function index(){
        $profile = Profile::all();
        return response(['list_profile' => $profile],200);
    }

    public function list_user(){
        $user = User::all();
        $response = ['list_user' => $user];
        return response($response, 200);
    }

    public function get_all_user(){
        $user = User::all();
        $response = ['list_all_user' => $user, 'status' => 'success'];
        
        return response($response, 200);
    }

    public function get_specified_profile($id){
        // $id = auth('api')->user()->id;
        $profile = Profile::where('id_user',$id)->get();

        return response(['User Profile' => $profile, 'status' => 'success'], 200);
    }

    public function get_specified_user($id){
        $user = User::find($id);

        return response(['User info' => $user, 'status' => 'success'], 200);
    }

    public function update_profile($id, Request $req){
        //get user id from auth
        //$id = auth('api')->user()->id;
        //$id_user = Profile::select('id_profile')->where('id_user',$id)->get();

        $profile = Profile::find($id);
        $validator = Validator::make($req->all(), [

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        //dd($profile);

        $profile->bio = $req->bio;
        $profile->team = $req->team;
        $profile->region = $req->region;
        $profile->school = $req->school;
        $profile->steam_id = $req->steam_id;
        $profile->followers = $req->followers;
        $profile->connections = $req->connections;
        $profile->rep_point = $req->rep_point;
        $profile->photo_link = $req->photo_link;
        $profile->video = $req->video;
        $profile->steam_link = $req->steam_link;
        $profile->save();

        //$profile = Profile::find();
        return response(['list_profile' => $profile, 'status' => 'profile success updated'],200);
    }
    public function update_user($id, Request $req){
        $user = User::findOrFail($id);

        $user->username = $req->username;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->tempat_lahir = $req->tempat_lahir;
        $user->tanggal_lahir = $req->tanggal_lahir;
        $user->jenis_kelamin = $req->jenis_kelamin;
        $user->alamat = $req->alamat;
        $user->save();

        return response(['status' => 'success', 'message' => 'User has been updated'], 200);
    }

    public function getProfile(){
        $profile = User::find(1);
        while($profile) {
            return response()->json([$profile,$profile->profile], 200);
        }
        //return response()->json([$profile, $profile->profile], 200);
    }

}
