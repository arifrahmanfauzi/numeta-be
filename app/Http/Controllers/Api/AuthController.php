<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Profile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $req){
        $validator = Validator::make($req->all(),
        [
         'username' => 'required|string|max:255',
         'email' => 'required|string|email|max:255|unique:users',
         'password' => 'required|string|min:6|confirmed',
         //password_confirmation
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $req['password']=Hash::make($req['password']);
        // dd($req->toArray());
        $user = User::create($req->toArray());

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        $response = ['token' => $token, 'users' => $user];

        $profile = new Profile;
        $profile->id_user = $user->id;
        $profile->save();
        
        return response($response, 200);
    }

    public function login(Request $req){
    $user = User::where('email',$req->email)->first();
    if ($user) {
        if (Hash::check($req->password, $user->password)) {
            $token = $user->createToken('createToken')->accessToken;
            $auth = User::where('email',$req->email)->firstOrFail();
            $response = ['token' => $token, 'status_login' => true, 'user' => $auth];
            return response($response, 200);
        }else{
            $response = 'Password Missmatch';
            return response($response, 422);
        }
        } else{
            $response = 'User Does not Exist';
            return response($response, 422);
        }
    }

    public function logout(Request $req){
        $token = $req->user()->token();
        $token->revoke();
        $response = 'You have been succesfully logged out!';
    return response($response, 200);
    }

    public function list_user(){
        $user = User::all();
        $response = ['list_user' => $user];
        return response($response, 200);
    }
}
